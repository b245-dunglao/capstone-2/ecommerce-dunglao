const mongoose = require("mongoose");

const ordersSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "userID is required!"]	
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "productId is required!"]	
			},
			qty: {
				type: Number,
				required: [true, "qty is required!"]
			},
			subtotal: {
				type: Number,
				required: [true, "subtotal is required!"]
			}
		}

	],
	totalAmount : {
		type: Number,
		required: [true, "totalAmount is required!"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	completed : {
		type: Boolean,
		default: false
	},

});


module.exports = mongoose.model("Order", ordersSchema);