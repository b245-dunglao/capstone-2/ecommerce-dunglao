const mongoose = require("mongoose");


const usersSchema = new mongoose.Schema({


	email : {
		type: String,
		required: [true, "email of user is required."]
	},
	password : {
		type: String,
		required: [true, "password of user is required."]
	},
	isAdmin : {
		type: Boolean,
		default: false
	},
	firstName : {
		type: String,
		required: [true, "firstName of user is required."]
	},
	lastName : {
		type: String,
		required: [true, "lastName of user is required."]
	},
	mobileNo : {
		type: String,
		required: [true, "mobileNo of user is required."]
	}
	



})


module.exports = mongoose.model("User", usersSchema);