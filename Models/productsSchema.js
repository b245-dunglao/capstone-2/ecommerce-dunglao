const mongoose = require("mongoose");

const productsSchema = new mongoose.Schema({

	name : {
		type: String,
		required: [true, "name of the product is required."]
	},
	description : {
		type: String,
		required: [true, "description of the product is required."]
	},
	imgLink : {
		type: String,
		required: [true, "description of the product is required."]
	},
	price : {
		type: Number,
		required: [true, "price of the product is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}

});


module.exports = mongoose.model("Product", productsSchema);