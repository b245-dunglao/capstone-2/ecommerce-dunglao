const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const port = 3001;
const app = express();


const userRoutes = require("./Routes/userRoutes.js");
const productRoutes = require("./Routes/productRoutes.js");
const orderRoutes = require("./Routes/orderRoutes.js");

mongoose.set('strictQuery', true);

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@b245-dunglao.tbtmdrd.mongodb.net/b245_capstone2_ecommerce_dunglao?retryWrites=true&w=majority", 
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Check connection
let db = mongoose.connection;

// Error catcher - Error handling
db.on("error", console.error.bind(console, "Connection Error."));

// Confirmation of the connection - Validation of the connection
db.once("open",()=> console.log("We are now connected to the cloud."));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// Routing
app.use("/user", userRoutes);
app.use("/product", productRoutes);
app.use("/order", orderRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}.`));