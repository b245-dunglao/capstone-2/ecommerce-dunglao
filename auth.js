const jsonwebtoken = require("jsonwebtoken");

const secret = "b245capstone2ecommerce";

module.exports.createAccessToken = (user) => {
	
	const data = {
		_id: user._id,
		email : user.email,
		isAdmin: user.isAdmin
	}
	return jsonwebtoken.sign(data, secret, {});
}

module.exports.verify = (request, response, next) =>{
	let token = request.headers.authorization; 

	if(typeof token !== "undefined"){
		token = token.slice(7,token.length)
		return jsonwebtoken.verify(token, secret, (err, data) => {
			if (err){
				return response.send({auth:"Failed."})
			}
			else {
				next();
			}
		});
	}
	else {
		return response.send({auth: "Failed"});
	}
}


module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
			return jsonwebtoken.verify(token, secret, (err, data)=>{
				if(err){
					return null;
				}
				else{
					return jsonwebtoken.decode(token, {complete: true}).payload;
				}
			})
	}
	else {
		return null;
	}
}