const User = require("../Models/usersSchema.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


// User registration
module.exports.userRegister = ( request, response ) => {
	const input = request.body;

	User.findOne({email: input.email})
		.then(result => {
			if(result !== null){
				return response.send(false)
			}
			else {
				let newUser = new User({

					email: input.email,
					password : bcrypt.hashSync(input.password, 10),
					firstName : input.firstName,
					lastName : input.lastName,
					mobileNo : input.mobileNo	
				})

				newUser.save()
					.then(save => {
						return response.send(true)
					})
			}
		})
		.catch(error => {
			return response.send(error)
		})
}


// User login
module.exports.userLogin = ( request, response ) => {
	let input = request.body;

		User.findOne({email: input.email})
		.then(result => {
			if (result === null) {
				// Encourage registration
				return response.send(false);
			}else {
				const passwordCheck = bcrypt.compareSync(input.password, result.password);

				if(passwordCheck){
					return response.send({auth: auth.createAccessToken(result)});

				}else{
					// Indirect advise of information is incorrect.
					return response.send(false);
				}
			}
		})
		.catch(error => {
			return response.send(error)
		})
}

// User view account 
module.exports.userAccount = ( request, response ) => {
	// gets data from token
	const userData = auth.decode(request.headers.authorization);
	// hide remove _id, __v, isAdmin
	User.findById(userData._id)
	.then(result =>{
		result.password = "**********";
		return response.send(result);
	})
}


// Admin account control (admin only)
module.exports.userModify = ( request, response ) => {
	const userData = auth.decode(request.headers.authorization);
	const id = request.params.id;
	const status = request.params.status;
	let code = ""

	if (userData.isAdmin && (status === "true" || status === "false")){

		if (status === "true"){
			code = {"isAdmin" : true}

		}
		else if ( status === "false"){
			code = {"isAdmin": false}
		}

		User.findByIdAndUpdate(id,code,{new:true})
			.then(result => {
				result.password = "**********";
				return response.send(result)
			})
			.catch(error => {
					return response.send(error)
			})


	}else{
		return response.send("Please contact your administrator")
	}

}