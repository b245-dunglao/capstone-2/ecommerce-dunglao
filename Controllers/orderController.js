const Order = require("../Models/ordersSchema.js");
const Product = require("../Models/productsSchema.js");
const User = require("../Models/usersSchema.js");
const auth = require("../auth.js");
const mongoose = require("mongoose");

// User checkout/create order
module.exports.checkOut = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const input = request.body;
	let totalPrice = 0;
	let productArray =[];


	if(!userData.isAdmin){
		Order.findOne({userId : userData._id,completed:false} )
		.then(result => {
			if (result){
				return response.send(result)
			}
			else{
				let newOrder = new Order({
						userId: userData._id,
						products : productArray,
						totalAmount : totalPrice,	
					})
					newOrder.save()
						.then(save => {
							Order.findOne({userId : userData._id,completed:false} )
								.then(result => {
									if (result){
										return response.send(result)
									}

								})
						})
			}
		})

		.catch(error => {
			return response.send(false)
		})
	}
	

}

// Add to cart - will add qty when existing and will add new line when not. can only accept correct productId
module.exports.addToCart = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const input = request.body;
	let totalPrice = 0;

		await Order.findOne({_id: input._id, userId: userData._id})
		.then(async result => {
			if (result !== null){


				if (result.completed){
					return response.send(false)

				}
				else {
					for (let count = 0; count < input.products.length; count++ ){

						await Product.findById(input.products[count].productId)
						.then( async productInfo => {

		
							let index = await result.products.findIndex(product => product.productId === input.products[count].productId);
							if (index === -1){
								result.products.push({ productId: input.products[count].productId, qty : input.products[count].qty, subtotal: (input.products[count].qty*productInfo.price)})

							}
							else {
								// splice the array to remove the value and update to the new value amount. 
								result.products.splice(index, 1, {productId: result.products[index].productId, qty : parseInt(input.products[count].qty) + parseInt(result.products[index].qty), subtotal: (result.products[index].qty*productInfo.price) });
							}	
						})
					}	
					await result.save();

					await Order.findOne({_id: input._id, userId: userData._id})
					.then( async orderResult =>{
						
						for (let count = 0; count < orderResult.products.length; count ++){
								totalPrice = totalPrice + orderResult.products[count].subtotal;
						}
					});
					await Order.findOneAndUpdate({_id: input._id, userId: userData._id}, {totalAmount : totalPrice}, {new:true})
					.then(completedOrder => response.send(true));
				}
			}
			else {
				return response.send(false);
			}
		})
		.catch(error => {
			return response.send(error)
		});

}

// Change 1 product qty if item was in the order and add as a new line if not. 
module.exports.updateItemQty = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
  	const input = request.body;
  	let totalPrice = 0;


  	await Order.findOne({_id: input._id, userId: userData._id})
		.then(async result => {
			
			if (result !== null){
				if (result.completed){
					return response.send(false)

				}
				else {
					await Product.findById(input.products[0].productId)
					.then( async productInfo => {

						let index = result.products.findIndex(product => product.productId === input.products[0].productId);

							if (index === -1){
								result.products.push({ productId: input.products[0].productId, qty : input.products[0].qty, subtotal: (input.products[0].qty*productInfo.price)})
							}
							else { 
								result.products.splice(index, 1, {productId: input.products[0].productId, qty : input.products[0].qty, subtotal: (input.products[0].qty*productInfo.price)});
							}	
							await result.save();

							await Order.findOne({_id: input._id, userId: userData._id})
							.then( async orderResult =>{
								
								for (let count = 0; count < orderResult.products.length; count ++){
										totalPrice = totalPrice + orderResult.products[count].subtotal;
								}
							});
							
							await Order.findOneAndUpdate({_id: input._id, userId: userData._id}, {totalAmount : totalPrice}, {new:true})
							.then(result => response.send(true));

					})
				}
			}
			else {
				return response.send(false)
			}

			
		})
		.catch(error => {
			return response.send(error)
		});
}


// Remove products from the cart
module.exports.removeItem = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
  	const input = request.body;
  	let totalPrice = 0;

  	

await Order.findOne({_id: input._id, userId: userData._id})
		.then(async result => {
			if (result !== null){
				if (result.completed === true){
					return response.send(false)

				}
				else {

				
					let index = result.products.findIndex(product => product.productId === input.productId);

					if (index === -1){
						return response.send(false)
					}
					else { 

						result.products.pop(index);
						await result.save();

						await Order.findOne({_id: input._id, userId: userData._id})
						.then( async orderResult =>{
							
							for (let count = 0; count < orderResult.products.length; count ++){
									totalPrice = totalPrice + orderResult.products[count].subtotal;
							}
						});

						await Order.findOneAndUpdate({_id: input._id, userId: userData._id}, {totalAmount : totalPrice}, {new:true})
						.then(result => response.send(true));

					}	
				}
					
			}
			else {
				return response.send(false)
			}

			
		})
		.catch(error => {
			return response.send(error)
		});

}


// Check user's own order w/ checkout
module.exports.reviewOrder = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const orderId = request.params.orderId;
	const action = request.params.action;

	if (!userData.isAdmin && mongoose.Types.ObjectId.isValid(orderId)) {
		Order.findById(orderId)
		.then(result => {
			if (result !== null && userData._id === result.userId){
				if (action === "checkout"){
					Order.findByIdAndUpdate(orderId,{completed:true},{new:true})
					.then(result => response.send(result))
				}
				else if(action === "view") {
					return response.send(result)
				}
				else {
					return response.send(false)
				}
			}
			else {
				return response.send(false)
			}
		})
	}
	else {
		return response.send(false)
	}
}


// Retrieve all orders ( admin only )
module.exports.reviewAllOrder = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const status = request.params.status;

		if ( status === "completed") {
			Order.find({userId: userData._id, completed: true}).then(result => response.send(result)).catch(error => response.send(error));
		}
		else if (status === "ongoing"){
			Order.find({userId: userData._id, completed: false}).then(result => response.send(result)).catch(error => response.send(error));
		}
		else if (status === "all") {
			Order.find({userId: userData._id}).then(result => response.send(result)).catch(error => response.send(error));
		}			
	
	
}


