const Product = require("../Models/productsSchema.js");
const auth = require("../auth.js");
const mongoose = require("mongoose");


// Create Product (admin only)
module.exports.addProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const input = request.body;

	if (userData.isAdmin === true){
		let newProduct = new Product({

			name: input.name,
			description : input.description	,
			price : input.price,
			imgLink : input.imgLink	
		})

		newProduct.save()
			.then(save => {
				return response.send(true)
			})
			.catch(error => {
				return response.send(error)
			})

	}else{
		return response.send(false)
	}

}



// Retrieve All Active Products 
module.exports.retrieveProducts = (request, response) => {
	const input = request.body;

	Product.find({isActive: true},{isActive:0, createdOn: 0, __v:0})
	.then(result => {
		return response.send(result	)
	})
	.catch(error => {
		return response.send(false)
	})
}

// Retrieve One Active Product
module.exports.retrieveOneProduct = (request, response) => {
	const input = request.params.idOrName ;
	let searchCode = "";

	if (mongoose.Types.ObjectId.isValid(input)) {
		searchCode = {_id : input, isActive:true };
	}
	else {
		searchCode = {name: input, isActive:true }
	}
	Product.find(searchCode,{isActive:0, createdOn: 0, __v:0})
	.then(result => {
		if(result !== null && result.length > 0){
			return response.send(result)
		}
		else{
			return response.send(false)
		}
	})
	.catch(error => {
		return response.send(error);
	})
}

// Retrieve All Products (admin only)
module.exports.retrieveAllProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const input = request.body;

	if (userData.isAdmin === true){
		Product.find({})
			.then(result => {
				return response.send(result	)
			})
			.catch(error => {
				return response.send(error)
			})
	}
	else{
		return response.send(false)
	}
}


// Retrieve one Products (admin only)
module.exports.retrieveOneProductUpdate = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;

	if (userData.isAdmin === true){
		Product.findById(productId)
			.then(result => {
				return response.send(result	)
			})
			.catch(error => {
				return response.send(error)
			})
	}
	else{
		return response.send(false)
	}
}

// Update product information (admin only)
module.exports.updateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const input = request.body;
	
	if (userData.isAdmin === true){
		Product.findByIdAndUpdate(input._id,{
								name : input.name,
								description : input.description,
								price : input.price,
								imgLink:input.imgLink
							}, {new:true})
		.then(result => {
			return response.send(result)
		})
		.catch(error => {
			return response.send(false)
		})


	}else{
		return response.send(false)
	}
}

// Archive product (admin only)
module.exports.archiveProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const input = request.body;

	if (userData.isAdmin === true){

		Product.findByIdAndUpdate(input._id,{
								isActive : input.isActive

							}, {new:true})
		.then(result => {
			return response.send(result)
		})
		.catch(error => {
			return response.send(error)
		})


	}else{
		return response.send(false);
	}
}
	
