const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const userController = require("../Controllers/userController.js")


// User registration
router.post("/register", userController.userRegister);

// User login
router.post("/login", userController.userLogin);

// User view account 
router.get("/account", auth.verify, userController.userAccount);

// Admin account control (admin only)
router.post("/modify/:id/:status", auth.verify, userController.userModify);

module.exports = router; 