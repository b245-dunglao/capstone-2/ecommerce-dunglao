const express = require("express");
const router = express.Router();
const auth = require("../auth.js");


const orderController = require("../Controllers/orderController.js");

// User checkout/create order
router.post("/checkOut", auth.verify, orderController.checkOut);

// Add to cart
router.put("/addItem", auth.verify, orderController.addToCart);

// Change product qty
router.put("/updateQty", auth.verify, orderController.updateItemQty);

//Remove product from cart
router.put("/removeItem", auth.verify, orderController.removeItem);

// Check user's own order /w checkout
router.put("/reviewOrder/:orderId/:action", auth.verify, orderController.reviewOrder);

// Retrieve all orders ( admin only )
router.get("/reviewAll/:status", auth.verify, orderController.reviewAllOrder);


module.exports = router; 	