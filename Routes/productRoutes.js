const express = require("express");
const router = express.Router();
const auth = require("../auth.js");


const productController = require("../Controllers/productController.js")


// Create Product (admin only)
router.post("/addProduct", auth.verify, productController.addProduct);

// Retrieve All Active Products
router.get("/searchAll", productController.retrieveProducts);

// Retrieve One Active Products
router.get("/searchOne/:idOrName", productController.retrieveOneProduct);

// Retrieve All Products (admin only)
router.get("/searchAllProduct",auth.verify, productController.retrieveAllProduct);

// Retrieve One Product (admin only)
router.get("/retrieveOne/:productId",auth.verify, productController.retrieveOneProductUpdate);
 
// Update product information (admin only)
router.post("/updateProduct", auth.verify, productController.updateProduct);

// Archive product (admin only)
router.post("/archiveProduct", auth.verify, productController.archiveProduct);


module.exports = router; 